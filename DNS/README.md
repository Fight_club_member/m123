# Modul 123 - Aufgabenstellung - „DNS Dienst zur Verfügung stellen“

## Was ist ein DNS? 
DNS steht für **Domain Name System** auch bekannt als das *Telefonbuch des Internets.* Es wandelt eine IP-Adresse in einem Websteite Namen um oder umgekehrt. **Beispiel:* Sie kennen den Namen der Person, wissen jedoch seine Nummer nicht. Im unseren Fall wissen wir die Webseite Adresse. (zb. Google.com) jedoch die IP-Adresse nicht. Diese Anfrage wird vom *DNS-Server* in die IP-Adresse umgewandelt. (8.8.8.8)

## Der Auftrag
Ich musste einen Windows Server mit DNS konfigurieren. Diesen dann mit einem Windows 10 Client verbinden. Diese Dinge musste ich beachten, dass der Auftrag richtig Funktionierte:
* Richtige IP Adresse konfigurenren.
* Richtige Versionen Installieren.
* Das Schema des *Bild* 1 in betracht ziehen. 
* Statische IP-Adresse notwendig.

Benutzte Software:
* Windows 10 Education
* Windows Server 2022 (Desktopversion)


*Bild 1* ![Alt text](image-4.png)

## Client
## *Installieren*
Als erstes habe ich den Client auf *VMware installiert. Da das Installieren von Windows für uns schon vertraut ist, kann man die Tutorials von Calisto nochmal anschauen. 
Link Tutorials: https://gitlab.com/ch-tbz-it/Stud/m117/-/tree/main/Leistungsbeurteilungen/LB2

Die Lizenzen für die Software habe ich über *Azure* beschaffen.

## *Netzwerk*
Damit sich der DNS Server und der Client sehen können. musste ich die richtigen Netzwerk konfiguratioen beachten. (*Bild 1*) Als erstes musste ich das Schema mit dem Client ergänzen. Also ich musste eine neue Netzwerkkarte hinzufügen.
um eine neue Netzwerkkarte hinzufügen muss man bei: **Settings**->**add**->**Network adapter** gehen.
Diese kann man dann konfigurieren und dann hinzufügen. man braucht 2 Netzwerkkarten, damit man das *Bild 1* Schema ergänzen kann.

*Bild 2* ![Alt text](image-5.png)

Nun musste ich 2 Netzwerke hinzufügen. dies habe ich bei **Edit**->**Virtual Netowrk Editor** gemacht. Dort habe ich dann 2 Netzwerken hinzugefügt. eine, mit eienr NAT (öffentlich) und die andere mit einer Statischen IP-Adresse.

*Bild 3*![Alt text](image-6.png)

Die IP-Adressen habe ich selber gewählt. Jedoch für den späteren Zeitpunkt sehr wichtig.

Nun habe ich beim Client 2 funktonierende Netzwerke.

*Bild 4* ![Alt text](image-7.png)

Diese findet man unter: *LAN-Zeichen unten rechts*->**Netzwerk & Interneteinstellungen**->**Ethernet**->**Adapteroptionen ändern** (*Bild 4*)
Danach *rechtscklick auf Ethernet 0*->**Eigentschaften**-> **Internetprotokoll, Version 4 (TCP/IPv4)**->**Eigenschaften**

*Bild 5*![Alt text](image-8.png)

Nun haben wir den NAT-Netzwerk vor uns. Hier musste ich nur den folgenden DNS-Server IP-Adresse eingeben. (Diese wird später bekannt gegeben)
Die *Alternativer DNS-Server* habe ich dem Googele Server zugeteilt. Welche ich im späterem Zeitpunkt erklären werde.

Nun machen wir dies auch beim **Ethernet 1**. (*Bild 6*)
Jedoch vergeben wir diesem eine statische IP-Adresse.
Der DNS bleibt gleich.

*Bild 6* ![Alt text](image-9.png)

Nun ist der Client fertig konfiguriert. (**Hinweis: Dies ist NICHT die richtige reihenfolge, da man ab einem bestimmten Zeitpunkt zwischen CLient und Server wechseln muss!**)

## DNS-Server
## *Installation*
Für die Installation habe ich eine Anleitung gewählt. (Hier geht es zur Anleitung: https://computingforgeeks.com/configure-dns-server-on-windows-server-2022/)

Die Software Lizenz habe ich ebenso auf *Azure* genommen.
Als ich mit de Installation ferig war, sah es so aus: (*Bild 7*) Es sollte nun ein DNS-Server angezeigt werden.

 *Bild 7* ![Alt text](image-10.png)

## *Konfigurieren

 Nun musste ich diese Konfigurieren.
 Zuerst musste ich *Forward-Lookupzone* und *Reverse-Lookuozone* konfigurieren. Diese finde ich unter: **Tools**->**DNS**

 *Bild 8* ![Alt text](image-11.png)

 ### Was ist Forward und Reverse-Lookupzonen?
 Diese sind dazu da, um IP-Adressen in Webseitnamen umzuwandeln und umgekehrt. 
 
 Zuerst habe ich bei beiden eine **Neue Zone** erstellt.
 Und dann mithilfe der Anleitung diese konfigueriert. (Bitte Beachten: **Umbedingt die IP verwenden, die man selber genutzt hat.**)
 In diesen Zonen musste ich einfach die IP-des Clients angeben. 

 Jetzt musste ich nur noch den Googel DNS hinzufügen, damit ich einen zwischen-Server habe. Mit diesem kann ich zu allen öffentlichen Webseiten zugreifen.

 unter **WIN**->**Eigenschaften**->**Weiterleitungen**->**bearbeiten**

*Bild 8* ![Alt text](image-12.png)

Nun musste ich bei dem blauen Kästchen klicken und eine gültige IP-Adresse eingeben. in diesem Fall 8.8.8.8 

Jetzt ist die Konfiguration vollendet, mal schauen ob diese auch Funktionieren.

## *Test
Das Testen erfolgt im Client, ich gehe auf *CMD* und gebe **Ipconfig /all** ein.

*Bild 9*![Alt text](image-14.png)
wie man sieht ist mein DNS-Server sichtbar.

*Bild 10* ![Alt text](image-15.png)

Nun können wir mit dem Befehl: **nslookup** genauer nachschauen.

*Bild 11* ![Alt text](image-16.png)

Wenn ich es richtig gemacht habe, kann ich jede IP-Adresse jeder Webseite herausfinden, wenn ich nur die Webseite Adresse eingebe.

*Bild 12* ![Alt text](image-17.png)


## *Fazit
Dieser Auftrag ist mir wesentlicher Leichter gefallen als der des DHCPs. Leider konnte ich Linux nicht richtig ausprobieren da ich privatlich keine Zeit hatte.
Alles in einem ist es mir gut gelungen. Jedoch ist Zeitlich wieder ein Problem. Da mir private Aufgaben mir immer im Weg kommen.
