# Ubuntu Server Samba Doku

### benutzte Software / Anleitung:
- Windows 10 Education
- Ubuntu Server 22.04
- https://www.youtube.com/watch?v=c0f71Wol8xc

## Der Auftrag
Ich musste auf Linux einen Samba Server einrichten. Diese musst ich mit filesshares konfigurieren. Das ganze soll ich dann auf einen Client testen. (ping / Fileshare)
## Was ist Samba?
Der Samba-Server wird über die Datei /etc/samba/smb.conf konfiguriert, welche mit einem Texteditor und nur mit Root-Rechten bearbeitet werden kann.

## 1. Installieren
damit lässt sich es installieren.
![Alt text](image.png)

## 2. New User erstellen
![Alt text](image-2.png)
![Alt text](image-3.png)

## 3. User dem Server hinzufugen. (Sharefolder)
![Alt text](image-4.png)

## 4. Folder erstellen
![Alt text](image-5.png)

## 5. Folder dem Samba zuweisen
mit diesem Befehl kann man auf die smb.conf zugreifen.

![Alt text](image-6.png)

![Alt text](image-8.png)
ganz nach unten scrollen. dann dies einfügen

![Alt text](image-9.png)

## 6. Samba Service neustarten
![Alt text](image-10.png)

## Test
1. ping: ![Alt text](image-11.png)

2. zugriff.
![Alt text](image-12.png)

![Alt text](image-13.png)

![Alt text](image-14.png)

## Fazit:
Es war sehr einfach für mich jedoch gab es viele technische schwierigkeiten. Ich musste lange auf bleiben und diese Probleme zu lösen.Aufgeben war für mich nie eine Lösung Denn: `Who is gonna carry the boats!?`

dhfgbbv