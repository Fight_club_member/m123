# DHCP-Dienst Auftrag in Filius Dokumentation
 ## Einleitung:
Mein Auftrag war es, **einen Client mit dem Switch zu verbinden, und diese im DHCP-Server zu setzen**. Dafür brauchte ich einen DHCP-Server, einen Switch und 3 Clients.


---
## 1.
Zuerst musste ich die Clients mit dem Switch verbinden und diese mit dem **DHCP-Server** verbinden.


![Architektur](./images/1.png)

---
## 2.
Danach musste ich Clients den DHCP *aktivieren.*
![Architektur](./images/2.png)
---
## 3.
Beim Client 3 ist es *statisch*, also darf ich diese nicht ändern. Nur bei Client 1 und 2 musste ich die IP noch angeben, die Anforderungen stehen neben dem DHCP-Server

![Architektur](./images/Bild.png)
---
## 4.

Jetzt müssen wir noch den DHCP-Server ***einstellen***. Wir müssen Client 3 **manuell** hinzufügen, weil es statisch ist. Nun müssen wir bei: «DHCP-Server einrichten».  Dann bei Hinzufügen drücken und nun diese *aktivieren*. (Client 3 wird nicht mit DHCP aktiviert)

![Architektur](./images/Bild%20(1).png)
---
## 5. 
Nun habe ich dies getestet. Beim Bild habe ich Client 1 mit 3 gepingt, alles läuft **reibungslos.**

![Architektur](./images/Bild%20(2).png) u





# Auftrag 2 

## einleitung 
In diesem Auftrag muss ich einen DHCP Server/Setup aufstellen. Dies mit genauer Anleitung. 

 zuerst musste ich Im Router diese 3 Befehle eingeben: 
- show ip dhcp pool
- show ip dhcp binding
- show running-config
Um heruauszufidnen im welche Subnetz der Server/Router ist.

! <img src="image.png" alt="drawing" width="300"/>

Diese sind dafür da, um dann später die Clients zu konfigurieren.
Als nächstes müssen wir
nun habe ich beim PC02 die Ipv4 auf static, dann wieder auf DHCP. Nun sind 2 Datenpakete entstanden.

! <img src="image-2.png" alt="drawing" width="400"/>

Danach konnte ich diese starten. Diese Pakete werden an alle Clients, Server, Switch etc. weitergeschikt. Alle bekommen keinen error, ausser den Router, da er einen DHCP hat. Diese werden zurück geschikt und danach wieder raus geschikt. Dieser Vorgang wird mehrmals wiederholt. (DORA)

! <img src="image-3.png" alt="drawing" width="400"/>

## Fragen:
- Welcher OP-Code hat der DHCP-Offer?
- Welcher OP-Code hat der DHCP-Request?
- Welcher OP-Code hat der DHCP-Acknowledge?
- An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser - IP-Adresse speziell?
- An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser - MAC-Adresse speziell?
- Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?

- Welche IPv4-Adresse wird dem Client zugewiesen?

Antworten:
- 0x0000000000000002
- 0x0000000000000001
- 0x0000000000000002
- 0002.169D.6901 (Router)
- um den PC02 zu indentifizieren.
- 192.168.23.1 

## DORA:

Discover: 

! <img src="image-4.png" alt="drawing" width="400"/>

Offer: 

! <img src="image-6.png" alt="drawing" width="400"/>

Der Dora vorgang ist, der wichtigste Teil in dieser SImulation.

D = Discover

O = Offer 

R = Reqest

A = Acnolage

Dieser VOrgang wird immer bei einem Dhcp *Server* eingesetzt.




