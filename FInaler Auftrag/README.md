# Finaler Auftrag mit Filius

## Auftrag 
Ich muss mit Filius alles zeigen, was ich gelernt habe. Eine Netzwerkinfrastruktur mit DHCP / DNS / Web-Server konfigurieren.

Ich musste mit diesem Schema arbeiten:

![Alt text](image-6.png)

## 1. Verbinden
Als erstes muss ich alles verbinden. **Wichtig!!** Der DHCP Server muss mit einem Switch verbunden sein.

![Alt text](image-7.png)

## 2. IP-Adressen verteilen
Anhand des Schemas muss ich die IP-Adressen verteilen, richtige Gateways.
Und die CLients mit **DHCP zu konfiguration verwenden.**

## 2. DHCP
Nun muss ich den DHCP konfigurieren. 
* Range einteilen
* IP vergeben
* Gateway vergeben
* DNS eingeben

![Alt text](image-8.png)

![Alt text](image-9.png)

Nun kurz testen:
Es sieht gut aus, jeder Client hat eine DHCP-IP bekommen.

![Alt text](image-10.png)

## 3. DNS
ALs nächstes muss ich den DNS konfigurieren.
Ich muss alle CLient dort eingeben

![Alt text](image-11.png)

um zu testen mach ich das:

![Alt text](image-12.png)

## 4. Web

Als letztes den Web-Dienst konfigurieren.

![Alt text](image-13.png)

![Alt text](image-14.png)

Nun wieder alles testen.

![Alt text](image-15.png)

Es klappt!